#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <time.h>

#include <cv.h>
#include <highgui.h>
#include "v4l2.h"
#include "serial_port.h"

using namespace cv;

#define FRAME_WIDTH		320
#define FRAME_HEIGHT	180
#define FRAME_DIAG		sqrt(pow(FRAME_WIDTH, 2) + pow(FRAME_HEIGHT, 2))
#define FRAME_SIZE 		FRAME_WIDTH * FRAME_HEIGHT
#define CAM_ANGL		4 * 0.213f

/* Global variables */
/* Images */	
IplImage* Yuv_img = 0;
IplImage* Can_img = 0;
IplImage* Dbg_img = 0;
IplImage* Res_img = 0;
char filename[30];

struct Line_elem
{
    CvPoint point[2];		//init and end point of line
    double teta;			//line angle to OX axis
    double k;				//tg(line angle)
    double b;				//line offset
    bool is_parallel;		//flag of line parallel of horizont
    bool deleted;			//flag of invalid line
};
volatile Line_elem* Line_arr = NULL;

struct Gray_count
{
	unsigned int j_ind;			//index of second line
    uint32_t gray;				//count of gray pixels between lines
    uint64_t othr;				//count of other pixels between lines
    double betta;				//angle between i and j lines
};
struct Zone
{
	unsigned int i_ind;			//index of first line
    unsigned int z_c;			//count of zones
    struct Gray_count* g_c;		//array of gray counted zones
};
volatile Zone* Zones = NULL;

struct Runway
{
	unsigned int l_ind;			//index of left Runway border
	double l_teta;				//left border angle
    unsigned int r_ind;			//index of right Runway border
    double r_teta;				//right border angle
    
    double dist;				//direct distance to Runway in pixels
    double lin_dist;			//direct distance to Runway in meters
    
    float delta_course;			//delta of Drone and Runway courses
    float course;				//Runway heading (in degrees)
};
volatile Runway Rw;				//current Runway info
bool Rw_is_found = false;		//flag of success

int vd = -1;					//videodevice file discriptor
unsigned int intensity = 0;		//the amount of non-zero pixels

unsigned int fr = 0;			//frame counter 
unsigned int fr_p = 0;			//previous value of frame counter 
int fps = 0;					//frames per second

uint16_t msg_reg = NONE_MSG;
struct Drone_Params Current_Drone_Params;

bool debug = false;				//flag of debugging
mavlink_status_t lastStatus;	//last operation status
volatile uint8_t state;			//state machine
volatile int seconds;			//seconds to fly to reach Runway;

//return min value of 4 arguments
int min4(int a, int b, int c, int d)
{
	int ab = fmin(a, b);
	int cd = fmin(c, d);
	
	int res = fmin(ab, cd);
	return res;
}

//return max value of 4 arguments
int max4(int a, int b, int c, int d)
{
	int ab = fmax(a, b);
	int cd = fmax(c, d);
	
	int res = fmax(ab, cd);
	return res;
}	

//init all memory storages
void Init_Images(CvSize s)
{
	Yuv_img = cvCreateImage(s, IPL_DEPTH_8U, 3);		//create YCbCr image
	Can_img = cvCreateImage(s, IPL_DEPTH_8U, 1);   		//create canny image
	Dbg_img = cvCreateImage(s, IPL_DEPTH_8U, 1);   		//create image for debug painting
	Res_img = cvCreateImage(s, IPL_DEPTH_8U, 1); 		//create result image
}

//Hough transform
int Hough_Analysis(void)
{
	int Lines_count = 0;								//zero lines count
    if (intensity != 0)									//if there is none-zero pixels
	{	
		memset(Dbg_img->imageData, 0, FRAME_SIZE);		//clear debug image	
		
		double area = FRAME_SIZE / intensity;			//count of pixels contained 1 non-zero pixel
		double radius = sqrt(area);						//radius of area 
		double ratio = 1.0f / radius;					//balance between non-zero and zero pixels
		
		if (debug)
		{
			printf("Intensity = %d, area = %f, radius = %f, ratio = %f\n", intensity, area, radius, ratio);
		}
		
		//calc hough transform parameters		
		double theta = CV_PI / 180;								//angular dimension [radians]
		double ro = 0.7f;										//linear dimension [pixels]
		double max_gap = radius;								//max distance between two points
		int thres = (int)(ratio * intensity);					//count of succes cross-cups - count of pixels in line
		double min_len = CV_PI * radius;						//min length of line
		if (thres == 0) thres = 1;
		if (thres > 2 * min_len) thres = (int)2 * min_len;
		
		if (debug)
		{
			printf("Hough transform params:\n theta = %f, ro = %f, max_gap = %f, thres = %d, min_len = %f\n",
					theta, ro, max_gap, thres, min_len);
		}

		CvMemStorage* store = cvCreateMemStorage(0);	//create memory storage for lines (64 KB)
		CvSeq* Lines = cvHoughLines2(Can_img, store, CV_HOUGH_PROBABILISTIC, ro, theta, thres, min_len, max_gap); //find lines
		Lines_count = Lines->total;
		if (Lines_count > 1)							//if amount of lines > 1
        {
			if (Line_arr != NULL)						//delete old lines data
			{
				delete[] Line_arr;
				Line_arr = NULL;
			}
            Line_arr = new Line_elem[Lines_count]; 		//create array of lines
	    	
	    	for (unsigned int i = 0; i < Lines_count; i++)
            {
				CvPoint* p = (CvPoint*)cvGetSeqElem(Lines, i);											//get init and end point of line
				Line_arr[i].point[0].x = p[0].x;
				Line_arr[i].point[0].y = p[0].y;
				Line_arr[i].point[1].x = p[1].x;
				Line_arr[i].point[1].y = p[1].y;
				//calc lines params in Hough system of notation 
                double dy = (Line_arr[i].point[1].y - Line_arr[i].point[0].y) / 1.0;					//calc dy 
                double dx = (Line_arr[i].point[1].x - Line_arr[i].point[0].x) / 1.0;					//calc dx 
                if (dx != 0)																			//line is not parallel to X axis
                {
					if (dy == 0) Line_arr[i].is_parallel = true;										//mark line as parallel of horizont
					Line_arr[i].k = dy / dx;															//calc tg(line angle)
					Line_arr[i].b = Line_arr[i].point[1].y - (Line_arr[i].k * Line_arr[i].point[1].x);	//calc line offset
					Line_arr[i].teta = atan2(dy, dx) * 180 / M_PI;										//calc line angle 
					if (Line_arr[i].teta >= 0) Line_arr[i].teta = 180.0f - Line_arr[i].teta;
					else Line_arr[i].teta = - Line_arr[i].teta;
				}
                else 																					//line is parallel to X axis
                {
					Line_arr[i].k = 0.0f;																//tg(line angle) = inf
					Line_arr[i].b = Line_arr[i].point[0].x;												//line equation is x = b;
					Line_arr[i].teta = 90.0f;															//set line angle 
				}
				Line_arr[i].deleted = false;
			
				if (debug)
				{
					printf("Line[%d] params:\n teta = %f, k = %f, b = %f\n", i, Line_arr[i].teta, Line_arr[i].k, Line_arr[i].b);
					
	                //drawing lines		
					uchar* ptr = (uchar*)Dbg_img->imageData;	//get the pointer to the first pixel of debug image		
					unsigned int x = Line_arr[i].point[0].x;
					while (x < Line_arr[i].point[1].x)
					{			
						unsigned int y = (unsigned int)(Line_arr[i].k * x + Line_arr[i].b);
						ptr[FRAME_WIDTH * y + x] = 255; 
						x++;
					} 
				}
			}
			if (debug)
			{
				memset(filename, 0, sizeof(filename));
				sprintf(filename, "dbg%d.bmp", fr);
				cvSaveImage(filename, Dbg_img);
			}
		}
		else Lines_count = 0;
		cvReleaseMemStorage(&store);	//release memory storage
	}
	return Lines_count;
}

//find result Runway info
void Result(int Lines_count)
{
	//concatenate lines
	unsigned int del_ls = 0; 							//the amount of deleted lines
	for (unsigned int i = 0; i < Lines_count - 1; i++)
	{
		if (Line_arr[i].deleted) continue;				//if flag marked go to next line
		for (unsigned int j = i + 1; j < Lines_count; j++)
		{
			if (Line_arr[j].deleted) continue;			//if flag marked go to next line
			
			double betta = 0.0f;						//calc angle between lines
			if (Line_arr[i].teta > Line_arr[j].teta) betta = Line_arr[i].teta - Line_arr[j].teta;
			else betta = Line_arr[j].teta - Line_arr[i].teta;
					
			if (betta < (90.0f / Current_Drone_Params.rel_alt)) //if the angle is smaller than angular resolution
			{
				if (Line_arr[j].point[0].x < Line_arr[i].point[0].x || 
					Line_arr[j].point[0].y < Line_arr[i].point[0].y)
				{
					Line_arr[i].point[0].x = Line_arr[j].point[0].x;
					Line_arr[i].point[0].y = Line_arr[j].point[0].y;
				}
				if (Line_arr[j].point[1].x > Line_arr[i].point[1].x || 
					Line_arr[j].point[1].y > Line_arr[i].point[1].y)
				{
					Line_arr[i].point[1].x = Line_arr[j].point[1].x;
					Line_arr[i].point[1].y = Line_arr[j].point[1].y;
				}
				Line_arr[j].deleted = true;							//mark the flag
				del_ls++;											//increase count of deleted lines
			}
		}
	}
		
	//start zones analysis
    struct Runway Cur_Rw;
    Zones = new Zone[Lines_count - del_ls - 1];						//init array of zones
	uint32_t gray_max = 0;											//zero the max gray pixel count
	uint32_t othr_min = FRAME_SIZE;									//zero the min other pixel count
	unsigned int f = 0, s = 0;										//first and second counter
    for (unsigned int i = 0; i < Lines_count - 1; i++)
	{
		if (Line_arr[i].deleted == true) continue;					//if flag marked go to next line
		if ((Lines_count - del_ls - 1 - f) > 0) 				//if it is not the last line
		{
			Zones[f].i_ind = i;										//save index of first line
			Zones[f].z_c = Lines_count - del_ls - 1 - f;			//count of zones 
			Zones[f].g_c = new Gray_count[Zones[f].z_c];			//init the array of zones 	
			
			s = 0;													//zero second counter	
			for (unsigned int j = i + 1; j < Lines_count; j++)
			{
				if (Line_arr[j].deleted == true) continue;			//if flag marked go to next line
				
				//if lines are parralel to horizont
				if (Line_arr[i].is_parallel && Line_arr[j].is_parallel) 
				{
					//they have not to stand far away to each other
					if (fabs(Line_arr[i].b - Line_arr[j].b) > (10 * FRAME_HEIGHT / Current_Drone_Params.rel_alt)) continue;
				}
				
				//if lines are perpendicular to horizont
				if ((!Line_arr[i].is_parallel && !Line_arr[j].is_parallel) &&
					(Line_arr[i].k == 0 && Line_arr[j].k == 0))
				{
					//they also have not to stand far away to each other
					if (abs(Line_arr[i].point[0].x - Line_arr[j].point[0].x) > (10 * FRAME_WIDTH / Current_Drone_Params.rel_alt)) continue;
				}
				
				double betta = 0.0f;						//calc angle between lines
				if (Line_arr[i].teta > Line_arr[j].teta) betta = Line_arr[i].teta - Line_arr[j].teta;
				else betta = Line_arr[j].teta - Line_arr[i].teta;
				//if angle between lines is too big
				if (betta > (1800.0f / Current_Drone_Params.rel_alt)) continue;
				
				//start analysis
				Zones[f].g_c[s].j_ind = j;							//save index of second line
				Zones[f].g_c[s].gray = 0;							//zero grey count
				Zones[f].g_c[s].othr = 0;							//zero othr count
	
				unsigned int x1_init = Line_arr[i].point[0].x;					//save x1 cord of init point
				unsigned int x1_end = Line_arr[i].point[1].x;					//save x1 cord of end point
				unsigned int x2_init = Line_arr[j].point[0].x;					//save x2 cord of init point
				unsigned int x2_end = Line_arr[j].point[1].x;					//save x2 cord of end point
				unsigned int x_init = min4(x1_init, x2_init, x1_end, x2_end);	//calc x cord of init point
				unsigned int x_end = max4(x1_init, x2_init, x1_end, x2_end);	//calc x cord of end point
				
				unsigned int y1_init = Line_arr[i].point[0].y;					//save y1 cord of init point
				unsigned int y1_end = Line_arr[i].point[1].y;					//save y1 cord of end point
				unsigned int y2_init = Line_arr[j].point[0].y;					//save y2 cord of init point
				unsigned int y2_end = Line_arr[j].point[1].y;					//save y2 cord of end point
				unsigned int y_init = min4(y1_init, y2_init, y1_end, y2_end);	//calc y cord of init point
				unsigned int y_end = max4(y1_init, y2_init, y1_end, y2_end);	//calc y cord of end point
				
				uchar* ptr_yuv = (uchar*)Yuv_img->imageData;					//get the pointer to the start of Yuv_img data
					
				unsigned int off_l = 3 * FRAME_WIDTH;							//line length of Yuv_img in bytes 
				unsigned int off_s = 3 * (y_init * FRAME_WIDTH + x_init);		//memory offset since the start of data
					
				unsigned int y = y_init;		//start the cycle of y
				while (y < y_end)		
				{
					unsigned int off = off_s;	
					int x = x_init;				//start the cycle of x
					while (x < x_end)
					{
						if (abs(ptr_yuv[off + 1] - ptr_yuv[off + 2]) < 10) 	//if the pixel is gray
							Zones[f].g_c[s].gray++;							//increase grey pixels count  
						else Zones[f].g_c[s].othr++;						//increase other pixels count  
						x++;												//next pixel
						off += 3;											//next pixel offset
					}
					y++;						//next line
					off_s += off_l;				//next line offset
				}
				if (Zones[f].g_c[s].gray > gray_max) gray_max = Zones[f].g_c[s].gray;	//save new max gray value
				s++;																	//increase second counter
			}
		}
		else break;
		f++;											//increase first counter
	}
	
	unsigned int i_max, j_max;
	for (unsigned int i = 0; i < f; i++)
	{
		for (unsigned int j = 0; j < Zones[i].z_c; j++)
		{
			if (Zones[i].g_c[j].gray == gray_max)
			{
				if (Zones[i].g_c[j].othr <= othr_min)
				{
					othr_min = Zones[i].g_c[j].othr;						//save new min other value
					
					i_max = Zones[i].i_ind;
					j_max = Zones[i].g_c[j].j_ind;
				}
			}
		}
	}
	
	//finally calc new course
	if (i_max != j_max && gray_max != 0)	//if border lines were found
	{
		double x0_i = 0.0f, x0_j = 0.0f;
		if (Line_arr[i_max].k != 0.0f) x0_i = - Line_arr[i_max].b / Line_arr[i_max].k;
		if (Line_arr[j_max].k != 0.0f) x0_j = - Line_arr[j_max].b / Line_arr[j_max].k;
		if (x0_i <= x0_j)
		{
			Cur_Rw.l_ind = i_max;										//save left line params
			Cur_Rw.l_teta = Line_arr[i_max].teta;
		
			Cur_Rw.r_ind = j_max;										//save right line params
			Cur_Rw.r_teta = Line_arr[j_max].teta;
		}
		else
		{
			Cur_Rw.l_ind = j_max;										//save left line params
			Cur_Rw.l_teta = Line_arr[j_max].teta;
		
			Cur_Rw.r_ind = i_max;										//save right line params
			Cur_Rw.r_teta = Line_arr[i_max].teta;
		}
		
		if (debug)
		{
			printf("Found Runway params:\n l_ind = %d, l_teta = %f, r_ind = %d, r_teta = %f\n", 
					Cur_Rw.l_ind, Cur_Rw.l_teta, Cur_Rw.r_ind, Cur_Rw.r_teta);
			
			//drawing result
			uchar* ptr = (uchar*)Can_img->imageData;
			
			unsigned int x = Line_arr[Cur_Rw.l_ind].point[0].x;
			while (x < Line_arr[Cur_Rw.l_ind].point[1].x)
			{			
				unsigned int y = (unsigned int)(Line_arr[Cur_Rw.l_ind].k * x + Line_arr[Cur_Rw.l_ind].b);
				ptr[FRAME_WIDTH * y + x] = 255; 
				x++;
			}
						
			x = Line_arr[Cur_Rw.r_ind].point[0].x;
			while (x < Line_arr[Cur_Rw.r_ind].point[1].x)
			{			
				unsigned int y = (unsigned int)(Line_arr[Cur_Rw.r_ind].k * x + Line_arr[Cur_Rw.r_ind].b);
				ptr[FRAME_WIDTH * y + x] = 255; 
				x++;
			}
			
			memset(filename, 0, sizeof(filename));
			sprintf(filename, "can%d.bmp", fr);
			cvSaveImage(filename, Can_img);
		}
		
		float c_fi = 0.0f;									//angle of course line
		float betta = Cur_Rw.l_teta - Cur_Rw.r_teta;
		
		if (betta < (90.0f / Current_Drone_Params.rel_alt))	//borders are parallel to each other and to the Runway direction
		{
			c_fi = Cur_Rw.l_teta;							//the course angle is the same
		}
		else
		{			
			if (Cur_Rw.l_teta <= 90.0f && Cur_Rw.r_teta > 90.0f) goto exit;
			c_fi = Cur_Rw.l_teta - (betta / 2.0f);
		}
		float sint = sin(M_PI - c_fi);
		if (sint > 0.0f && sint < 1.0f) Cur_Rw.dist = (float)((FRAME_WIDTH / 2.0) / sint);
		else if (sint == 0.0f) Cur_Rw.dist = (float)(FRAME_HEIGHT - (Line_arr[Cur_Rw.r_ind].point[0].y - Line_arr[Cur_Rw.l_ind].point[0].y) / 2.0);
		else Cur_Rw.dist = (float)(FRAME_WIDTH - (Line_arr[Cur_Rw.r_ind].point[0].x - Line_arr[Cur_Rw.l_ind].point[0].x) / 2.0);
		
		Cur_Rw.lin_dist = Cur_Rw.dist * (Current_Drone_Params.rel_alt * CAM_ANGL / FRAME_DIAG);	//calc linear distance to Runway
		Cur_Rw.delta_course = 90.0f - c_fi;														//calc Runway course delta
		Cur_Rw.course = Current_Drone_Params.heading - Cur_Rw.delta_course;						//calc Runway course angle
		
		if (Cur_Rw.lin_dist > 0)
		{
			seconds = Cur_Rw.lin_dist / Current_Drone_Params.g_speed;							//calc time to fly
			memcpy((void*)&Rw, (const void*)&Cur_Rw, sizeof(struct Runway));					//save data
			state = 0x04;																		//go to the fly correction state
		}
		
		if (debug)
		{
			printf("Result:\n");
			printf(" Delta course = %f\n", Cur_Rw.delta_course);
			printf(" Runway course = %f\n", Cur_Rw.course);
			printf(" Linear distance = %f\n", Cur_Rw.lin_dist);
			printf(" Time to fly = %d\n", seconds);
		}	
	}									

exit:	
    //deleting all resources
	for (unsigned int i = 0; i < f; i++) delete[] Zones[i].g_c;
	delete[] Zones;
	Zones = NULL;
}

void read_message(char* Buf, int len)
{
	mavlink_message_t Rx_msg;					//input message from APM
	mavlink_status_t status;					//current message status
	uint8_t          msgReceived = false;
	
	for (int i = 0; i < len; i++)
	{
		msgReceived = mavlink_parse_char(MAVLINK_COMM_1, Buf[i], &Rx_msg, &status);
		
		if(msgReceived)
		{			
			uint8_t buffer[BUFLEN];
		
			// check message is write length
			unsigned int messageLength = mavlink_msg_to_send_buffer(buffer, &Rx_msg);
			
			// no message length error
			if (messageLength <= BUFLEN)
			{
				msg_reg |= Parse_MavLINK_message(Rx_msg);
			}
		}
	}
}

//communication with APM function
int write_message(uint32_t mask)
{
	mavlink_message_t message;		//output packed message
	
	mavlink_command_int_t cmd;		//internal casual message
	cmd.target_system    = sysid;
	cmd.target_component = autopilot_compid;
	
	//prepare internal message depending on input value
	switch (mask)
	{
		//set alt
		case ALT:
		{
			cmd.frame = MAV_FRAME_MISSION;
			cmd.command = MAV_CMD_CONDITION_CHANGE_ALT;
			cmd.current = true;
			cmd.autocontinue = false;
			cmd.param1 = 1.0f;
			cmd.param2 = 0.0f;
			cmd.param3 = 0.0f;
			cmd.param4 = 0.0f;
			cmd.x = 0.0f;
			cmd.y = 0.0f;
			cmd.z = 300.0f;
			break;
		}
		//set course
		case COURSE:
		{
			cmd.frame = MAV_FRAME_MISSION;
			cmd.command = MAV_CMD_CONDITION_YAW;
			cmd.current = true;
			cmd.autocontinue = false;
			cmd.param1 = Rw.course;
			cmd.param2 = fabs(Rw.delta_course / seconds);
			if (Rw.delta_course > 0) cmd.param3 = 1.0f;
			else cmd.param3 = -1.0f;
			cmd.param4 = 0.0f;
			cmd.x = 0.0f;
			cmd.y = 0.0f;
			cmd.z = 0.0f;
			break;
		}
		//start landing
		case LANDING:
		{
			cmd.frame = MAV_FRAME_MISSION;
			cmd.command = MAV_CMD_DO_LAND_START;
			cmd.current = true;
			cmd.autocontinue = false;
			cmd.param1 = 0.0f;
			cmd.param2 = 0.0f;
			cmd.param3 = 0.0f;
			cmd.param4 = 0.0f;
			cmd.x = Current_Drone_Params.lat;
			cmd.y = Current_Drone_Params.lon;
			cmd.z = 0.0f;
			break;
		}
	}
	
	//encode internal message
	mavlink_msg_command_int_encode(sysid, compid, &message, &cmd);
	
	//write packed message in serial port
	int len = write_MAVLink(message);

	//done!
	return 0;
}

//waiting APM message function
void Waiting(uint16_t condition, uint8_t old_state, uint8_t new_state)
{
	int delay = 1000000;
	
	switch (condition)
	{
		case INITIAL:
		{
			//wait while APM config
			while ((msg_reg & CRIT_MSG) != CRIT_MSG)
			{
				delay--;
				//if APM does not response
				if (delay < 0) break;
			}
			break;
		}
		case COMMAND:
		{
			//wait user command 
			while (Current_Drone_Params.raw_RCchan5 > 1200)
			{
				delay--;
				//if APM does not response
				if (delay < 0) break;
			}
			break; 
		}
		case ALT:
		{
			//wait no pitch
			while (Current_Drone_Params.rel_alt > 300)
			{
				delay--;
				//if APM does not response
				if (delay < 0) break;
			}
			break;
		}
		case FLYING:
		{
			//wait flying
			while (abs(Current_Drone_Params.heading - Rw.course) > 3)
			{
				delay--;
				//if APM does not response
				if (delay < 0) break;
			}
			break;
		}
		case LANDING:
		{
			//wait landing
			while (Current_Drone_Params.rel_alt > 10)
			{
				delay--;
				//if APM does not response
				if (delay < 0) break;
			}
			break;
		}
	}
	
	if (delay >= 0) state = new_state;
	else state = old_state;
}

//enable or disable SIGIO 
void setup_Rx_signal(uint8_t enabled)
{
	sigset_t simask;
	
	sigemptyset(&simask);
	sigaddset(&simask, SIGIO); 								//add SIGIO
	if (enabled) sigprocmask(SIG_UNBLOCK, &simask, NULL); 	//unlock SIGIO
	else sigprocmask(SIG_BLOCK, &simask, NULL); 			//lock SIGIO
	return;				
}

//timer and termination signals handler
void sig_hdl(int sig)
{
	//program termination
	if (sig == SIGINT || sig == SIGTERM)	
	{
		printf("Termination signal recieved. Exit.\n");
		
		//free all images
		cvReleaseImage(&Yuv_img);
		cvReleaseImage(&Can_img);
		cvReleaseImage(&Dbg_img);
		cvReleaseImage(&Res_img);
			
		//set off the timer 
		setitimer(ITIMER_REAL, NULL, NULL); 
		
		//close serial port
		close_serial();
		
		//stop streaming
		if ((StreamOff() < 0) || (Close_all() < 0)) exit(EXIT_FAILURE);
		else exit(EXIT_SUCCESS);
	}
	//timer
	if (sig == SIGALRM)		
	{
		//if we recieved all needed messages
		if ((msg_reg & (ATTITUDE_MSG | GLOBALPOS_MSG))) 
		{
			//get parsed data
			Set_Drone_Params(&Current_Drone_Params);
			if (debug) Print_Drone_Params(Current_Drone_Params);
			//clean up reg
			msg_reg = NONE_MSG;
		}
		if (debug && state == 0x03)
		{
			//save images					
			memset(filename, 0, sizeof(filename));
			sprintf(filename, "can%d.bmp", fr);		
			cvSaveImage(filename, Can_img);
		}
		if (state == 0x05) seconds--;
		
		//calc FPS
		fps = fr - fr_p;
		printf("Current FPS: %d\n", fps);
		fr_p = fr;
	}
}
			
//application start point
int main (int argc, char* argv[])
{	
	uint8_t debug_visio;
	//get debugging flag
	if (argc > 1) debug = true;
	if (argc > 2) debug_visio = true;
	
	//configuring video subsystem
	//init resizer(IPIPEIF)
	int rs = Init_resizer(IMP_MODE_CONTINUOUS);
	//init previewer (IPIPE)
	int pr = Init_previewer(IMP_MODE_CONTINUOUS);
	//init capture (ISIF)
	int cc = Capture_Configure();
	//init display (RSZA)
	CvSize s = Display_Configure();
	//check errors, init memory buffers and start streaming
	if ((rs < 0) || (pr < 0) || (cc < 0) || (s.width != FRAME_WIDTH) || (s.height != FRAME_HEIGHT) || 
		(Mmap_Cam_Bufs() < 0) || (Alloc_Bufs(Mmap_Out_Bufs()) < 0) || (QueryBufs() < 0) || (StreamOn() < 0))
	{ 
		printf("An error occured during camera setup. The program will be terminated.\n");
		sig_hdl(SIGTERM);
	}
	else printf("Streaming on!\n");
	
	//signal handler initialization
	struct sigaction s_act;	
	sigset_t s_mask;
	memset(&s_act, 0, sizeof(s_act));
	s_act.sa_handler = sig_hdl;
	sigemptyset(&s_mask);                                                             
	sigaddset(&s_mask, (SIGINT | SIGTERM | SIGALRM)); 
	s_act.sa_mask = s_mask;
	sigaction(SIGINT, &s_act, NULL);
	sigaction(SIGTERM, &s_act, NULL);
	sigaction(SIGALRM, &s_act, NULL);
	
	//open serial port
	open_serial("/dev/ttyS1", read_message, &lastStatus);
	
	//init OpenCV images of current resolution
	Init_Images(s);
	
	//zero frame counter
	fr = 0;
	//zero state machine
	state = 0x00;
	
	if (debug_visio)
	{
		state = 0x03;
		Current_Drone_Params.rel_alt = 300;
		Current_Drone_Params.g_speed = 25.0f;
		struct itimerval tval;
		timerclear(&tval.it_interval); 
		timerclear(&tval.it_value);
		tval.it_value.tv_sec = 1; 		
		tval.it_interval.tv_sec = 1;
		setitimer(ITIMER_REAL, &tval, NULL);
	}
		
	//infinit loop
	while(1)
	{
		//state machine
		switch (state)
		{
			//init APM state
			case 0x00:		
			{
				if (debug) printf("Waiting APM data\n");
				//wait until APM answer
				Waiting(INITIAL, 0x00, 0x01);
				//if data recieved
				if (state == 0x01)
				{
					Set_Drone_Params(&Current_Drone_Params);
					//timer setup	
					struct itimerval tval;
					timerclear(&tval.it_interval); 
					timerclear(&tval.it_value);
					tval.it_value.tv_sec = 1; 		
					tval.it_interval.tv_sec = 1;
					setitimer(ITIMER_REAL, &tval, NULL);
				}
				break;
			}
			//state of waiting of landing command
			case 0x01:		
			{
				if (debug) printf("Waiting user command\n");
				//flying mode tumbler has to be set to stabilize  
				Waiting(COMMAND, 0x01, 0x02);
				break;
			}
			//state of waiting low horizontal fly
			case 0x02:		
			{
				if (debug) printf("Waiting low altitude\n");
				//send command to change altitude
				write_message(ALT);
				//waitinf low fly
				Waiting(ALT, 0x02, 0x03);
				break;
			}
			//state of finding runway
			case 0x03:
			{
				if (debug) printf("Finding Runway\n");
				//disable SIGIO handler
				setup_Rx_signal(false);
				//reading video frame
				intensity = Read_Bufs(Yuv_img->imageData, Can_img->imageData);	
				//make Hough analysis
				int h = Hough_Analysis();
				//find result										
				if (h > 1) Result(h);											
				fr++;
				break;
			}
			//state of setting course
			case 0x04:		
			{
				if (debug) printf("Set course\n");
				if (debug_visio)
				{
					state = 0x03;
					break;
				}
				//enable SIGIO handler
				setup_Rx_signal(true);
				//write the setpoint message
				write_message(COURSE);
				//go to flying state
				state = 0x05;
				break;
			}
			//stae of waiting while flying or time is out
			case 0x05:
			{
				if (debug) printf("Flying\n");
				//if time is not out fly to the runway
				if (seconds > 0) Waiting(FLYING, 0x05, 0x06);
				else 
				{
					printf("Landing failture! Try again\n");
					state =  0x03;
				}
				break;
			}
			//state of landing
			case 0x06:		
			{
				if (debug) printf("Landing\n");
				//write landing command
				write_message(LANDING);
				//wait until landing
				Waiting(LANDING, 0x06, 0x00);
				break;
			}
		}
	}	
	//the program shouldn't reach this point
	sig_hdl(SIGTERM);	
}
