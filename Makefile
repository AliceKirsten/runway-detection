CXX= /home/fiamma/virt2real-sdk/codesourcery/arm-2013.05/bin/arm-none-linux-gnueabi-g++
TARGET=$(shell basename 'pwd')
SOURCES=$(wildcard *.cpp)
CFLAGS	:= -I/usr/local/include -I/usr/local/include/opencv -I/usr/local/dm365/linux-2.6-master/include -I/usr/local/mavlink/v1.0 -L/usr/local/arm/lib -Wl,-rpath,/usr/local/arm/lib
OBJECTS=$(SOURCES:%.cpp=%.0)
LIBRARIES	:= -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_imgcodecs -lpthread -lm

.PHONY: all c

all: $(TARGET)

$(OBJECTS): $(SOURCES)

$(TARGET): $(OBJECTS)
	$(CXX) $(CFLAGS) -o RD RD.cpp v4l2.cpp serial_port.cpp system_ids.cpp $(LIBRARIES)  

c:
	rm -f ./RD
