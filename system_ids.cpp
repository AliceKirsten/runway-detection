/** Mavlink messages parser  **/
/** almost messages included **/

#include "system_ids.h"

Mavlink_Messages current_messages;

uint16_t Parse_MavLINK_message(mavlink_message_t message)
{
	uint16_t msg_code = NONE_MSG;
	
	// Handle Message ID
	switch (message.msgid)
	{
		//id = #0
		case MAVLINK_MSG_ID_HEARTBEAT:  
		{
			//printf("MAVLINK_MSG_ID_HEARTBEAT\n");
			mavlink_msg_heartbeat_decode(&message, &(current_messages.heartbeat));
			msg_code = HEARTBIT_MSG;
			
			break;
		}
		//id = #1
		case MAVLINK_MSG_ID_SYS_STATUS:
		{
			//printf("MAVLINK_MSG_ID_SYS_STATUS\n");
			mavlink_msg_sys_status_decode(&message, &(current_messages.sys_status));
			msg_code = SYSSTAT_MSG;
			
			break;
		}
		//id = #2
		case MAVLINK_MSG_ID_SYSTEM_TIME:
		{
			//printf("MAVLINK_MSG_ID_SYSTEM_TIME\n");
			mavlink_msg_system_time_decode(&message, &(current_messages.time));

			break;
		}
		//id = #24
		case MAVLINK_MSG_ID_GPS_RAW_INT:
		{
			//printf("MAVLINK_MSG_ID_GPS_RAW_INT\n");
			mavlink_msg_gps_raw_int_decode(&message, &(current_messages.gps_raw));

			break;
		}
		//id = #27
		case MAVLINK_MSG_ID_RAW_IMU:
		{
			//printf("MAVLINK_MSG_ID_RAW_IMU\n");
			mavlink_msg_raw_imu_decode(&message, &(current_messages.imu));

			break;
		}
		//id = #29
		case MAVLINK_MSG_ID_SCALED_PRESSURE:
		{
			//printf("MAVLINK_MSG_ID_SCALED_PRESSURE\n");
			mavlink_msg_scaled_pressure_decode(&message, &(current_messages.pressure));

			break;
		}
		//id = #30
		case MAVLINK_MSG_ID_ATTITUDE:
		{
			//printf("MAVLINK_MSG_ID_ATTITUDE\n");
			mavlink_msg_attitude_decode(&message, &(current_messages.attitude));
			msg_code = ATTITUDE_MSG;
			
			break;
		}
		//id = #33
		case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
		{
			//printf("MAVLINK_MSG_ID_GLOBAL_POSITION_INT\n");
			mavlink_msg_global_position_int_decode(&message, &(current_messages.global_position_int));
			msg_code = GLOBALPOS_MSG;
			
			break;
		}
		//id = #34
		case MAVLINK_MSG_ID_RC_CHANNELS_SCALED:
		{
			//printf("MAVLINK_MSG_ID_RC_CHANNELS_SCALED\n");
			mavlink_msg_rc_channels_scaled_decode(&message, &(current_messages.rc_channels_scaled));
			msg_code = RCSCALED_MSG;
			
			break;
		}
		//id = #35
		case MAVLINK_MSG_ID_RC_CHANNELS_RAW:
		{
			//printf("MAVLINK_MSG_ID_RC_CHANNELS_RAW\n");
			mavlink_msg_rc_channels_raw_decode(&message, &(current_messages.rc_channels_raw));
			msg_code = RCRAW_MSG;
			
			break;
		} 
		//id = #36
		case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW:
		{
			//printf("MAVLINK_MSG_ID_SERVO_OUTPUT_RAW\n");
			mavlink_msg_servo_output_raw_decode(&message, &(current_messages.servo_output_raw));
			
			break;
		}
		//id = #42
		case MAVLINK_MSG_ID_MISSION_CURRENT:
		{
			//printf("MAVLINK_MSG_ID_MISSION_CURRENT\n");
			mavlink_msg_mission_current_decode(&message, &(current_messages.mission));
			
			break;
		}
		//id = #62
		case MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT:
		{
			//printf("MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT\n");
			mavlink_msg_nav_controller_output_decode(&message, &(current_messages.nav));
			
			break;
		}
		//id = #74
		case MAVLINK_MSG_ID_VFR_HUD:
		{
			//printf("MAVLINK_MSG_ID_VFR_HUD\n");
			mavlink_msg_vfr_hud_decode(&message, &(current_messages.vfr_hud));
			
			break;
		}
		//id = #90
		case MAVLINK_MSG_ID_HIL_STATE:
		{
			//printf("MAVLINK_MSG_ID_HIL_STATE\n");
			mavlink_msg_hil_state_decode(&message, &(current_messages.hil_state));
			msg_code = HILSTATE_MSG;
			
			break;
		}
		//id = #91
		case MAVLINK_MSG_ID_HIL_CONTROLS:
		{
			//printf("MAVLINK_MSG_ID_HIL_CONTROLS\n");
			mavlink_msg_hil_controls_decode(&message, &(current_messages.hil_controls));
			msg_code = HILCONTROL_MSG;
			
			break;
		}
		//id = #109
		case MAVLINK_MSG_ID_RADIO_STATUS:
		{
			//printf("MAVLINK_MSG_ID_RADIO_STATUS\n");
			mavlink_msg_radio_status_decode(&message, &(current_messages.radio_status));

			break;
		}
		//id = #253
		case MAVLINK_MSG_ID_STATUSTEXT:
		{
			//printf("MAVLINK_MSG_ID_STATUSTEXT\n");
			mavlink_msg_statustext_decode(&message, &(current_messages.statustext));
			printf("Status text: %s\n", current_messages.statustext.text);
			msg_code = STATUSTEXT_MSG;
			
			break;
		}
		
		default:
		{
			printf("Warning, did not handle message id %i\n", message.msgid);
			break;
		}
	}
	
	return msg_code;
};

void Set_Drone_Params(Drone_Params* Drone)
{
	Drone->yaw = current_messages.attitude.yaw * 180 / M_PI;
	Drone->roll = current_messages.attitude.roll * 180 / M_PI;
	Drone->pitch = current_messages.attitude.pitch * 180 / M_PI;
	Drone->heading = current_messages.global_position_int.hdg / 100;
	
	Drone->lat = current_messages.global_position_int.lat / 10000000;
	Drone->lon = current_messages.global_position_int.lon / 10000000;
	Drone->alt = current_messages.global_position_int.alt / 1000;
	Drone->rel_alt = current_messages.global_position_int.relative_alt / 1000;
	Drone->g_speed = sqrtf(pow(current_messages.global_position_int.vx / 100, 2) + 
						   pow(current_messages.global_position_int.vy / 100, 2));
	
	Drone->raw_RCchan5 = current_messages.rc_channels_raw.chan5_raw;
	Drone->scaled_RCchan5 = current_messages.rc_channels_scaled.chan5_scaled;
	
	Drone->mode = current_messages.hil_controls.mode;
	Drone->nav_mode = current_messages.hil_controls.nav_mode;
	Drone->system_status = current_messages.heartbeat.system_status;
}

void Print_Drone_Params(Drone_Params Drone)
{
	printf("Current Drone params:\n");
	printf("- Euler angles\n");
	printf("	yaw = %f\n", Drone.yaw);
	printf("	roll = %f\n", Drone.roll);
	printf("	pitch = %f\n", Drone.pitch);
	printf("	heading = %f\n", Drone.heading);
	
	printf("- Absolute coordinates:\n");
	printf("	latitude = %d\n", Drone.lat);
	printf("	longitude = %d\n", Drone.lon);
	printf("	altitude = %d\n", Drone.alt);
	printf("	relative altitude = %d\n", Drone.rel_alt);
	printf("	ground speed = %f\n", Drone.g_speed);
	
	printf("- RC command tumbler value:\n");
	printf("	raw = %d\n", Drone.raw_RCchan5);
	printf("	scaled = %d\n", Drone.scaled_RCchan5);
	
	printf("- System parameters:\n");
	printf("	mode = %d\n", Drone.mode);
	printf("	navigation mode = %d\n", Drone.nav_mode);
	printf("	system status = %d\n", Drone.system_status);
}
