/**********************************************************************
 * DaVinci Media 365 Image processor configuration library.
 * In case of computer vision technology only.
 * (c) KPU MIREA
 * ********************************************************************/
 
 #include <stdio.h>   				/* Standard input/output definitions */
 #include <stdint.h>  				/* Standart integer types defenitions */
 #include <string.h>				/* String operations library */
 #include <fcntl.h>	  				/* File control definitions */
 #include <unistd.h>  				/* UNIX standard function definitions */
 #include <errno.h>					/* Error definitions */
 #include <sys/ioctl.h>				/* System input/output calls */
 #include <sys/types.h>				/* Standart types */
 #include <sys/mman.h>				/* Memory mapping */
 #include "opencv2/core/types_c.h"	/* OpenCV types definition */
 
 /* Davinci specific kernel headers */
#include <media/davinci/imp_common.h>
#include <media/davinci/imp_previewer.h>
#include <media/davinci/imp_resizer.h>
#include <media/davinci/davinci_vpfe.h>
#include <media/davinci/ccdc_dm365.h>
#include <media/davinci/dm365_ipipe.h>
 
 #define YEE_TABLE_FILE		"EE_Table.txt"

/***********************************************************************
 * NF config params
 ***********************************************************************/
static struct prev_nf nf_params = 
{
	.en = 1,
	.gr_sample_meth = IPIPE_NF_BOX,
	.shft_val = 3,
	.spread_val = 5,
	.apply_lsc_gain = 0,
	.thr = { 120, 130, 135, 140, 150, 160, 170, 200 },
	.str = { 16, 16, 15, 15, 15, 15, 15, 15 },
	.edge_det_min_thr = 0,
	.edge_det_max_thr = 2047
};

/***********************************************************************
 * YUV 422 conversion config params
 ***********************************************************************/
static struct prev_yuv422_conv yuv422_params = 
{
	.en_chrom_lpf = 1,
	.chrom_pos = IPIPE_YUV422_CHR_POS_COSITE
};

/***********************************************************************
 * Luminance adjustment config params
 ***********************************************************************/
static struct prev_lum_adj lum_adj_params = 
{
	.brightness = 0,
	.contrast = 32
};

/***********************************************************************
 * Edge Enhancement config params
 ***********************************************************************/
static short yee_table[MAX_SIZE_YEE_LUT];
static struct prev_yee yee_params = 
{
	.en = 1,
	.en_halo_red = 0,
	.merge_meth = IPIPE_YEE_EE_ES,
	.hpf_shft = 6,
	// Signed 10
	.hpf_coef_00 = (128 & 0x3FF),	
	.hpf_coef_01 = (64 & 0x3FF),
	.hpf_coef_02 = (-64 & 0x3FF),
	.hpf_coef_10 = (64 & 0x3FF),
	.hpf_coef_11 = (0 & 0x3FF),
	.hpf_coef_12 = (-32 & 0x3FF),
	.hpf_coef_20 = (-64 & 0x3FF),
	.hpf_coef_21 = (-32 & 0x3FF),
	.hpf_coef_22 = (0 & 0x3FF),
	.yee_thr = 63,
	.es_gain = 128,
	.es_thr1 = 768,
	.es_thr2 = 63,
	.es_gain_grad = 0,
	.es_ofst_grad = 0,
	.table = yee_table
};

/****************	Functions *****************************************/

int parse_yee_table(short* table);	/* Parse 2DEE LUT table */
int Close_all();					/* Close all opened device files */

int Init_resizer(int mode);			/* Config resizer */
int Set_previewer_param(unsigned short id, void* params);			/* Config IPIPE blocks params*/
int Init_previewer(int mode); 		/* Config IPIPE */
int Capture_Configure();			/* Config ISIF */
CvSize Display_Configure();			/* Config RSZA */
int Mmap_Cam_Bufs();				/* Camera bufs memory mapping */
int Mmap_Out_Bufs();				/* Display bufs memory mapping */
int Alloc_Bufs(int mmap);			/* Alloc image array */
int QueryBufs();					/* Create queue	*/
int StreamOn();						/* Start streaming */
int StreamOff();					/* Stop streaming */
unsigned int Read_Bufs(char* start_color, char* start_grey); 		/* Get captured bufer and process it */
unsigned int Capture(char* start_color, char* start_grey, int f);	/* Capture OpenCV image from image array */
