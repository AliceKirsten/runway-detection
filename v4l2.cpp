#include "v4l2.h"

int vc_fd = -1, vd_fd = -1, rsz_fd = -1, prv_fd = -1;
int map = -1, ptr = -1;

struct app_buf_type 
{
    void *start;
    int offset;
    int length;
} *cam_bufs, *out_bufs;

struct In_Image
{
	unsigned int w;
	unsigned int h;
	unsigned int bpp;
	unsigned int pf;
	unsigned int s;
	unsigned int c;
} in_img;

struct Out_Image
{
	unsigned int w;
	unsigned int h;
	unsigned int bpp;
	unsigned int pf;
	unsigned int l;
	unsigned int ll;
	unsigned int rsz;
	unsigned int c;
	uint16_t*** data;
} out_img;

int parse_yee_table(short* table)
{
	int ret = -1, val, i;
	FILE *fp;

	fp = fopen(YEE_TABLE_FILE, "r");
	if (fp != NULL) 
	{
		for (i = 0; i < MAX_SIZE_YEE_LUT; i++) 
		{
			fscanf(fp, "%d", &val);
			table[i] = val & 0x1FF;
		}
		if (i == MAX_SIZE_YEE_LUT) ret = 0;
		fclose(fp);
	}
	return ret;
}

int Close_all()
{
	if (vc_fd > 0) close(vc_fd);
	if (vd_fd > 0) close(vd_fd);
	if (rsz_fd > 0) close(rsz_fd);
	if (prv_fd > 0) close(prv_fd);
	for (unsigned int i = 0; i < out_img.c; i++)
	{
		if (cam_bufs[i].start && (cam_bufs[i].start != MAP_FAILED)) 
			munmap(cam_bufs[i].start, cam_bufs[i].length);
		if (out_bufs[i].start && (out_bufs[i].start != MAP_FAILED)) 
			munmap(out_bufs[i].start, out_bufs[i].length);
	}
	if (out_img.data != NULL) delete[] out_img.data;
	free(cam_bufs);
	free(out_bufs);
	return 0;
}

int Init_resizer(int mode)
{
    unsigned int user_mode = mode;
    unsigned int oper_mode;
    struct rsz_channel_config rsz_chan_config;
    struct rsz_continuous_config rsz_cont_config;
    struct rsz_single_shot_config rsz_ss_config;
    
    rsz_fd = open("/dev/davinci_resizer", O_RDWR);
    if(rsz_fd < 0) 
    {
        printf("Cannot open resize device\n");
        return -1;
    }
    else printf("Resizer '/dev/davinci_resizer' opened.\n");

    if (ioctl(rsz_fd, RSZ_S_OPER_MODE, &user_mode) < 0) 
    {
        perror("Can't set operation mode");
        return -1;
    }

    if (ioctl(rsz_fd, RSZ_G_OPER_MODE, &oper_mode) < 0) 
    {
        perror("Can't get operation mode");
        return -1;
    }

    if (oper_mode != user_mode) 
    {
        printf("failed to set mode to continuous in resizer\n");
        return -1;
    }
    else printf("Successfully set mode to continuous in resizer\n");
    
    // set configuration to chain resizer with preview
    rsz_chan_config.oper_mode = user_mode;
    rsz_chan_config.chain     = 2;
    rsz_chan_config.len       = 0;
    rsz_chan_config.config    = NULL; /* to set defaults in driver */
    if (ioctl(rsz_fd, RSZ_S_CONFIG, &rsz_chan_config) < 0) 
    {
        perror("Error in setting default configuration in resizer");
        return -1;
    }
    else
    {
		printf("Default configuration setting in Resizer:\n");
		printf("Operating mode = %d\n", rsz_chan_config.oper_mode);
		printf("Chain = %d\n", rsz_chan_config.chain);
		printf("Length = %d\n", rsz_chan_config.len);
	}		
    
    if (mode == IMP_MODE_CONTINUOUS)
    {
	    memset(&rsz_cont_config, 0, sizeof(struct rsz_continuous_config));    
	    rsz_chan_config.oper_mode = user_mode;
	    rsz_chan_config.chain     = 2;
	    rsz_chan_config.len       = sizeof(struct rsz_continuous_config);
	    rsz_chan_config.config    = &rsz_cont_config;
	    if (ioctl(rsz_fd, RSZ_G_CONFIG, &rsz_chan_config) < 0) 
	    {
	        perror("Error in getting resizer channel configuration from driver");
	        return -1;
	    }
	    
	    // set output specs
	    rsz_cont_config.output1.enable = 1;		//first output PAL
	    rsz_cont_config.output1.en_down_scale = 1;
	    rsz_cont_config.output1.h_dscale_ave_sz = IPIPE_DWN_SCALE_1_OVER_4;
	    rsz_cont_config.output1.v_dscale_ave_sz = IPIPE_DWN_SCALE_1_OVER_4;  
	    out_img.rsz = 4;
	    
	    rsz_cont_config.output2.enable = 0;		//second output 
	    rsz_chan_config.len = sizeof(struct rsz_continuous_config);
	    rsz_chan_config.config = &rsz_cont_config;
	    if (ioctl(rsz_fd, RSZ_S_CONFIG, &rsz_chan_config) < 0) 
	    {
	        perror("Error in setting configuration in resizer");
	        return -1;
	    }	
	}
	else
	{
		memset(&rsz_ss_config, 0, sizeof(struct rsz_single_shot_config));    
	    rsz_chan_config.oper_mode = user_mode;
	    rsz_chan_config.chain     = 2;
	    rsz_chan_config.len       = sizeof(struct rsz_single_shot_config);
	    rsz_chan_config.config    = &rsz_ss_config;
	    if (ioctl(rsz_fd, RSZ_G_CONFIG, &rsz_chan_config) < 0) 
	    {
	        perror("Error in getting resizer channel configuration from driver");
	        return -1;
	    }
	    
	    // set output specs
	    rsz_ss_config.output1.enable = 1;		//first output PAL
	    rsz_ss_config.output1.pix_fmt = IPIPE_UYVY;
	    rsz_ss_config.output1.en_down_scale = 1;
	    rsz_ss_config.output1.h_dscale_ave_sz = IPIPE_DWN_SCALE_1_OVER_4;
	    rsz_ss_config.output1.v_dscale_ave_sz = IPIPE_DWN_SCALE_1_OVER_4;  
	    out_img.rsz = 4;
	    
	    rsz_ss_config.output2.enable = 0;		//second output 
	    rsz_chan_config.len = sizeof(struct rsz_continuous_config);
	    rsz_chan_config.config = &rsz_ss_config;
	    if (ioctl(rsz_fd, RSZ_S_CONFIG, &rsz_chan_config) < 0) 
	    {
	        perror("Error in setting configuration in resizer");
	        return -1;
	    }
	}		
    printf("Resizer initialized\n");
    return rsz_fd;
}

int Set_previewer_param(unsigned short id, void* params)  
{	
	struct prev_cap cap;
	struct prev_module_param mod_param;
	
	cap.index = 0;
	while (1) 
	{
		int ret = ioctl(prv_fd , PREV_ENUM_CAP, &cap);
		if (ret < 0) break;	// find the defaults for this module
		
		if (cap.module_id == id)
		{			
			strcpy(mod_param.version, cap.version);
			mod_param.module_id = cap.module_id;
			mod_param.param = params;
			
			// try set parameter for this module
			if (id == PREV_NF1) mod_param.len = sizeof(struct prev_nf);
			else if (id == PREV_NF2) mod_param.len = sizeof(struct prev_nf);
			else if (id == PREV_WB) mod_param.len = sizeof(struct prev_wb);
			else if (id == PREV_LUM_ADJ) mod_param.len = sizeof (struct prev_lum_adj); 
			else if (id == PREV_GAMMA) mod_param.len = sizeof (struct prev_gamma);
			else if (id == PREV_RGB2YUV) mod_param.len = sizeof (struct prev_rgb2yuv);
			else if (id == PREV_YUV422_CONV) mod_param.len = sizeof (struct prev_yuv422_conv);
			else if (id == PREV_YEE) mod_param.len = sizeof (struct prev_yee);
			else mod_param.param = NULL;

			if (ioctl(prv_fd, PREV_S_PARAM, &mod_param) < 0) 
			{
				printf("Error in setting %s previewer module from driver\n", cap.module_name);
				return -1;
			}
			else 
			{
				printf("Parameters for %s prviewer module successfully changed\n", cap.module_name);
				return 0;
			}
		}
		cap.index++;  
	}	
}

int Init_previewer(int mode)
{
    unsigned int user_mode = mode;
    unsigned int oper_mode;
    struct prev_channel_config prev_chan_config;
    struct prev_continuous_config prev_cont_config; // continuous mode
    struct prev_single_shot_config prev_ss_config;
	
	prv_fd = open("/dev/davinci_previewer", O_RDWR);
    if(prv_fd < 0) 
    {
        printf("Cannot open previewer device\n");
        return -1;
    }
    else printf("Resizer '/dev/davinci_previewer' opened.\n");

    if (ioctl(prv_fd, PREV_S_OPER_MODE, &user_mode) < 0) 
    {
        perror("Can't set operation mode\n");
        return -1;
    }

    if (ioctl(prv_fd, PREV_G_OPER_MODE, &oper_mode) < 0) 
    {
        perror("Can't get operation mode\n");
        return -1;
    }

    if (oper_mode != user_mode) 
    {
        printf("failed to set mode to continuous in resizer\n");
        return -1;
    }
	else printf("Operating mode changed successfully to continuous in previewer\n");

    prev_chan_config.oper_mode = oper_mode;
    prev_chan_config.len = 0;
    prev_chan_config.config = NULL; /* to set defaults in driver */
    if (ioctl(prv_fd, PREV_S_CONFIG, &prev_chan_config) < 0) 
	{
        perror("Error in setting default configuration\n");
        return -1;
    }
    else
    {
		printf("Default configuration setting in Previewer:\n");
		printf("Operating mode = %d\n", prev_chan_config.oper_mode);
		printf("Length = %d\n", prev_chan_config.len);
	}
	
	if (mode == IMP_MODE_CONTINUOUS)
	{
	    prev_chan_config.oper_mode = oper_mode;
	    prev_chan_config.len = sizeof(struct prev_continuous_config);
	    prev_chan_config.config = &prev_cont_config;
		if (ioctl(prv_fd, PREV_G_CONFIG, &prev_chan_config) < 0) 
	    {
	        perror("Error in getting configuration from driver\n");
	        return -1;
	    }
	    
	    prev_chan_config.oper_mode = oper_mode;
	    prev_chan_config.len = sizeof(struct prev_continuous_config);
	    prev_chan_config.config = &prev_cont_config;
	    
	    /* configuring input specs */
	    prev_cont_config.input.avg_filter_en = 1;				//average filter enable
	    prev_cont_config.input.colp_elep= IPIPE_GREEN_RED;
		prev_cont_config.input.colp_elop= IPIPE_RED;
		prev_cont_config.input.colp_olep= IPIPE_BLUE;
		prev_cont_config.input.colp_olop= IPIPE_GREEN_BLUE;
	    if (ioctl(prv_fd, PREV_S_CONFIG, &prev_chan_config) < 0) 
	    {
	        perror("Error in setting default configuration\n");
	        return -1;
	    }
	    else
	    {
			printf("Configuration setting in Previewer:\n");
			printf("Operating mode = %d\n", prev_chan_config.oper_mode);
			printf("Length = %d\n", prev_chan_config.len);
		}	
	}
	else
	{
		prev_chan_config.oper_mode = oper_mode;
	    prev_chan_config.len = sizeof(struct prev_single_shot_config);
	    prev_chan_config.config = &prev_ss_config;
		if (ioctl(prv_fd, PREV_G_CONFIG, &prev_chan_config) < 0) 
	    {
	        perror("Error in getting configuration from driver\n");
	        return -1;
	    }
	    
	    prev_chan_config.oper_mode = oper_mode;
	    prev_chan_config.len = sizeof(struct prev_single_shot_config);
	    prev_chan_config.config = &prev_ss_config;
	    
	    /* configuring input specs */
	    prev_ss_config.input.avg_filter_en = 1;				//average filter enable
	    prev_ss_config.input.colp_elep= IPIPE_GREEN_RED;
		prev_ss_config.input.colp_elop= IPIPE_RED;
		prev_ss_config.input.colp_olep= IPIPE_BLUE;
		prev_ss_config.input.colp_olop= IPIPE_GREEN_BLUE;
		prev_ss_config.input.image_width = in_img.w;
		prev_ss_config.input.image_height = in_img.h;
		prev_ss_config.input.ppln= in_img.w * 1.5;
		prev_ss_config.input.lpfr = in_img.h + 10;
	    if (ioctl(prv_fd, PREV_S_CONFIG, &prev_chan_config) < 0) 
	    {
	        perror("Error in setting default configuration\n");
	        return -1;
	    }
	    else
	    {
			printf("Configuration setting in Previewer:\n");
			printf("Operating mode = %d\n", prev_chan_config.oper_mode);
			printf("Length = %d\n", prev_chan_config.len);
		}
	}
	
	if (parse_yee_table(yee_table) < 0)
	{
		printf("Error in parsing 2DEE LUT table\n");
		return -1;
	}

	if (Set_previewer_param(PREV_NF1, &nf_params) < 0 ||
		Set_previewer_param(PREV_NF2, &nf_params) < 0 ||
		Set_previewer_param(PREV_YUV422_CONV, &yuv422_params)< 0 ||
		Set_previewer_param(PREV_LUM_ADJ, &lum_adj_params) < 0 || 
		Set_previewer_param(PREV_YEE, &yee_params) < 0)
	{
		printf("Error in setup IPIPE blocks\n");
		return -1;
	}

	printf("Previewer initialized\n");
    return prv_fd;
}

int Capture_Configure()
{
	int ret = -1;
	struct v4l2_capability cap;
	struct v4l2_input cam;
	struct v4l2_standard std;
	struct v4l2_format img_fmt;
	
	vc_fd = open("/dev/video0", O_RDWR);
	if (vc_fd < 0)
	{
		printf("Can't open video capture device\n");
		return -1;
	}
	else printf("Video capture device '/dev/video0' opened.\n");
	
	if (ioctl(vc_fd, VIDIOC_QUERYCAP, &cap) != -1)
	{
		if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) && !(cap.capabilities & V4L2_CAP_DEVICE_CAPS))
		{
			printf("Device 'dev/video0' is not a capture device\n");
			perror("VIDIOC_QUERYCAP");
			return ret;
		}
		if (cap.capabilities & V4L2_CAP_STREAMING)
		{
			printf("Camera device detected. Memory mapping buffer allocation supported.\n");
			map = 1;
		}
	}

	memset(&cam, 0, sizeof(cam));
	if (ioctl(vc_fd, VIDIOC_G_INPUT, &cam.index) != -1) 
	{	
		if (ioctl(vc_fd, VIDIOC_ENUMINPUT, &cam) != -1) 
		{
			printf("cam.name = %s\n", cam.name);
			printf("cam.index = %d\n", cam.index);
			printf("0x%x video standart.\n", cam.std);
		}
		
		if (ioctl(vc_fd, VIDIOC_S_INPUT, &cam.index) == -1) 
		{
			printf("Can't set video input\n");
			perror("VIDIOC_S_INPUT");
			return ret;
		}
		else printf("Video input #%d is set\n", cam.index);
	}
	else
	{
		printf("Can't find camera\n");
		perror("VIDIOC_G_INPUT");
		return ret;
	}

	img_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (ioctl(vc_fd, VIDIOC_G_FMT, &img_fmt) != -1)
	{
		printf("Input image format:\n");
		printf("Width: %d\n", img_fmt.fmt.pix.width);
		printf("Height: %d\n", img_fmt.fmt.pix.height);
		printf("Pixelformat: 0x%x\n", img_fmt.fmt.pix.pixelformat);
		printf("Bytes per pixel: %d\n", img_fmt.fmt.pix.bytesperline / img_fmt.fmt.pix.width);
		printf("Sizeimage in bytes: %d\n", img_fmt.fmt.pix.sizeimage);
	}
	img_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	img_fmt.fmt.pix.width = 1280;
	img_fmt.fmt.pix.height = 720;
	img_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
	if (ioctl(vc_fd, VIDIOC_TRY_FMT, &img_fmt) != -1)
	{
		if (ioctl(vc_fd, VIDIOC_S_FMT, &img_fmt) != -1)
		{
			printf("Image format set:\n");
			printf("Width: %d\n", img_fmt.fmt.pix.width);
			printf("Height: %d\n", img_fmt.fmt.pix.height);
			printf("Pixelformat: 0x%x\n", img_fmt.fmt.pix.pixelformat);
			printf("Bytes per pixel: %d\n", img_fmt.fmt.pix.bytesperline / img_fmt.fmt.pix.width);
			printf("Sizeimage in bytes: %d\n", img_fmt.fmt.pix.sizeimage);
			in_img.w = img_fmt.fmt.pix.width;
			in_img.h = img_fmt.fmt.pix.height;
			in_img.bpp = img_fmt.fmt.pix.bytesperline / img_fmt.fmt.pix.width;
			in_img.pf = img_fmt.fmt.pix.pixelformat;
		}
	}

	ret = 1;
	return ret;
}

CvSize Display_Configure()
{
	int res_fd = -1, prev_fd = -1;
	struct v4l2_capability cap;
	struct v4l2_format img_fmt;
	
	CvSize res = cvSize(1, 1);
	
	vd_fd = open("/dev/video2", O_RDWR);
	if (vd_fd < 0)
	{
		printf("Can't open video display device\n");
		return res;
	}
	else printf("Video display device '/dev/video2' opened.\n");
	
	if (ioctl(vd_fd, VIDIOC_QUERYCAP, &cap) != -1)
	{
		if (!(cap.capabilities & V4L2_CAP_VIDEO_OUTPUT) && !(cap.capabilities & V4L2_CAP_DEVICE_CAPS))
		{
			printf("Device 'dev/video0' is not a display device\n");
			perror("VIDIOC_QUERYCAP");
			return res;
		}
		if (cap.capabilities & V4L2_CAP_STREAMING)
		{
			printf("Display device detected.\n");
			ptr = 1;
		}
	}

	img_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	if (ioctl(vd_fd, VIDIOC_G_FMT, &img_fmt) != -1)
	{
		printf("Output image format:\n");
		printf("Width: %d\n", img_fmt.fmt.pix.width);
		printf("Height: %d\n", img_fmt.fmt.pix.height);
		printf("Bytes per pixel: %d\n", img_fmt.fmt.pix.bytesperline / img_fmt.fmt.pix.width);
		printf("Sizeimage in bytes: %d\n", img_fmt.fmt.pix.sizeimage);
		printf("Pixelformat: 0x%x\n", img_fmt.fmt.pix.pixelformat);
	}
	img_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	img_fmt.fmt.pix.width = in_img.w / out_img.rsz;
	img_fmt.fmt.pix.height = in_img.h / out_img.rsz;
	img_fmt.fmt.pix.pixelformat = in_img.pf;
	img_fmt.fmt.pix.bytesperline = img_fmt.fmt.pix.width * in_img.bpp;
	img_fmt.fmt.pix.sizeimage = img_fmt.fmt.pix.bytesperline * img_fmt.fmt.pix.height; 
	if (ioctl(vd_fd, VIDIOC_TRY_FMT, &img_fmt) != -1)
	{
		if (ioctl(vd_fd, VIDIOC_S_FMT, &img_fmt) != -1)
		{
			printf("Image format set:\n");
			printf("Width: %d\n", img_fmt.fmt.pix.width);
			printf("Height: %d\n", img_fmt.fmt.pix.height);
			printf("Pixelformat: 0x%x\n", img_fmt.fmt.pix.pixelformat);
			printf("Bytes per pixel: %d\n", img_fmt.fmt.pix.bytesperline / img_fmt.fmt.pix.width);
			printf("Sizeimage in bytes: %d\n", img_fmt.fmt.pix.sizeimage);
			out_img.w = img_fmt.fmt.pix.width;
			out_img.h = img_fmt.fmt.pix.height;
			out_img.bpp = img_fmt.fmt.pix.bytesperline / img_fmt.fmt.pix.width;
			out_img.l = img_fmt.fmt.pix.sizeimage;
			out_img.ll = img_fmt.fmt.pix.bytesperline;
			out_img.pf = img_fmt.fmt.pix.pixelformat;
		}
	}
	res = cvSize(out_img.w, out_img.h);
	return res;
}
	
int Mmap_Cam_Bufs()
{
    if (map)
    {
		struct v4l2_requestbuffers reqbuf;
		
		memset(&reqbuf, 0, sizeof(reqbuf));
		reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		reqbuf.memory = V4L2_MEMORY_MMAP;
		reqbuf.count = 4;
		
		if (ioctl(vc_fd, VIDIOC_REQBUFS, &reqbuf) != -1)
		{
			printf("%d camera buffers requested.\n", reqbuf.count);
			in_img.c = reqbuf.count;
						
			cam_bufs = (app_buf_type*)calloc(reqbuf.count, sizeof(*cam_bufs));
			assert(cam_bufs != NULL);
			
			for (unsigned int i = 0; i < reqbuf.count; i++) 
			{
				struct v4l2_buffer buf;
				memset(&buf, 0, sizeof(buf));
				buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
				buf.memory = V4L2_MEMORY_MMAP;
				buf.index = i;
	
				if (ioctl (vc_fd, VIDIOC_QUERYBUF, &buf) == -1) 
				{	
					printf("Can't query camera buf!\n");
					perror("VIDIOC_QUERYBUF");
					return -1;
				}
				cam_bufs[i].length = buf.length;
				cam_bufs[i].start = mmap(NULL, buf.length,
										 PROT_READ | PROT_WRITE,		/* recommended */
										 MAP_SHARED,    				/* recommended */
										 vc_fd, buf.m.offset);
				cam_bufs[i].offset = buf.m.offset;
				if (cam_bufs[i].start == MAP_FAILED)
				{
					printf("Mmap failed!\n");
					free((void*)cam_bufs);
					perror("MMAP");
					return -1;
				}
			}
			printf("Camera memory mapping done.\n");
			return 0;
		}
		else 
		{
			printf("Can't request camera buffers.\n");
			perror("VIDIOC_REQBUFS");
			return -1;
		}
    }
	printf("Querying input buffers done.\n");
}

int Mmap_Out_Bufs()
{
    if (ptr)
    {
		struct v4l2_requestbuffers reqbuf;
		
		memset(&reqbuf, 0, sizeof(reqbuf));
		reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		reqbuf.memory = V4L2_MEMORY_USERPTR;
		reqbuf.count = 5;
		
		if (ioctl(vd_fd, VIDIOC_REQBUFS, &reqbuf) != -1)
		{
			printf("%d display buffers requested.\n", reqbuf.count);
			out_img.c = reqbuf.count;
			
			out_bufs = (app_buf_type*)calloc(reqbuf.count, sizeof(*out_bufs));
			assert(out_bufs != NULL);
			
			for (unsigned int i = 0; i < in_img.c; i++) 
			{
				out_bufs[i].length = out_img.l;
				out_bufs[i].start = cam_bufs[i].start;
				out_bufs[i].offset = cam_bufs[i].offset;		
			}
			if (out_img.c > in_img.c)
			{
				out_bufs[in_img.c].length = out_img.l;
				out_bufs[in_img.c].start = out_bufs[in_img.c - 1].start + out_bufs[in_img.c - 1].offset;
				out_bufs[in_img.c].offset = out_bufs[in_img.c - 1].offset;
			}
			printf("Display memory mapping done.\n");			
			return 0;
		}
		else 
		{
			printf("Can't request display buffers.\n");
			perror("VIDIOC_REQBUFS");
			return -1;
		}
    }
	printf("Querying output buffers done.\n");
}

int Alloc_Bufs(int mmap)
{
	if (!mmap)
	{
		out_img.data = new uint16_t**[out_img.c];
		for (unsigned int i = 0; i < out_img.c; i++) 
		{
			out_img.data[i] = new uint16_t*[out_img.h];
			for (unsigned int j = 0; j < out_img.h; j++)
			{
				out_img.data[i][j] = new uint16_t[out_img.w];
				out_img.data[i][j] = (uint16_t*)out_bufs[i].start + (j * in_img.w);
			}
		}
		printf("%d buffers of type YUV allocated.\n", out_img.c);
		return 0;	
	}
	else return -1;
}

int QueryBufs()
{
	struct v4l2_buffer buf;
	
	for (unsigned int i = 0; i < in_img.c; i++)
	{
		memset(&buf, 0, sizeof(buf));
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.length = cam_bufs[i].length;
		buf.m.offset = cam_bufs[i].offset;
		buf.index = i;
		if (ioctl(vc_fd, VIDIOC_QBUF, &buf) == -1) 
		{
			perror("CAPTURE: VIDIOC_QBUF");
			return -1;
		}
	}
	memset(&buf, 0, sizeof(buf));
	buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	buf.memory = V4L2_MEMORY_USERPTR;
	buf.length = out_bufs[in_img.c].length;
	buf.m.userptr = (unsigned long)out_bufs[in_img.c].start;
	buf.index = in_img.c;
	if (ioctl(vd_fd, VIDIOC_QBUF, &buf) == -1) 
	{
		perror("DISPLAY: VIDIOC_QBUF");
		return -1;
	}
	return 0;
}

int StreamOn()
{
	enum v4l2_buf_type type;
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (ioctl(vc_fd, VIDIOC_STREAMON, &type) == -1)
	{
		printf("Can't initialize capture streaming.\n");
		perror("VIDIOC_STREAMON");
		return -1;
	}
	type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	if (ioctl(vd_fd, VIDIOC_STREAMON, &type) == -1)
	{
		printf("Can't initialize display streaming.\n");
		perror("VIDIOC_STREAMON");
		return -1;
	}
	return 0;
}

int StreamOff()
{
	enum v4l2_buf_type type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (ioctl(vc_fd, VIDIOC_STREAMOFF, &type) == -1)
	{
		printf("Can't stop capture streaming.\n");
		perror("VIDIOC_STREAMOFF");
		return -1;
	}
	type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	if (ioctl(vd_fd, VIDIOC_STREAMOFF, &type) == -1)
	{
		printf("Can't stop display streaming.\n");
		perror("VIDIOC_STREAMOFF");
		return -1;
	}
	return 0;		
}

unsigned int Read_Bufs(char* start_color, char* start_grey)
{
	struct v4l2_buffer buf;
	unsigned int index = 0;
	
	unsigned int intensity = 0;
	
	memset(&buf, 0, sizeof(buf));
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	do
	{
		ioctl(vc_fd, VIDIOC_DQBUF, &buf);
	} while (errno == EAGAIN);
	index = buf.index;
	
	buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	buf.memory = V4L2_MEMORY_USERPTR;
	buf.length = out_bufs[buf.index].length;
	buf.m.userptr = (unsigned long)out_bufs[buf.index].start;
	ioctl(vd_fd, VIDIOC_QBUF, &buf);
	
	do
	{
		ioctl(vd_fd, VIDIOC_DQBUF, &buf);
	} while (errno == EAGAIN);
	
	intensity = Capture(start_color, start_grey, buf.index);
	
	ioctl(vd_fd, VIDIOC_QBUF, &buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	ioctl(vc_fd, VIDIOC_QBUF, &buf);
	
	return intensity;
}

unsigned int Capture(char* start_color, char* start_grey, int f)
{
	unsigned int pix_count = 0;
	unsigned int x, y;
	unsigned int off1, off2;
	unsigned int off_c = out_img.w * 3;
	unsigned int off_g = out_img.w;
	
	uchar* ptr_c = (uchar*) start_color; 
	uchar* ptr_g = (uchar*) start_grey;
	
	uchar Y = 0;
	
	y = 0;
	while (y < out_img.h)
	{
		x = 0;
		off1 = 0;
		
		while (x < out_img.w) 
		{
			off2 = off1 + 3;
			
			Y = (uchar)((out_img.data[f][y][x] & 0xff00) >> 8);			//Y1
			if (Y > 32)
			{
				ptr_c[off1] = ptr_g[x] = Y;
				pix_count++;		
			}
			else ptr_c[off1] = ptr_g[x] = 0;
			ptr_c[off1 + 2] = ptr_c[off2 + 2] = (uchar)(out_img.data[f][y][x] & 0x00ff);		//V
			Y = (uchar)((out_img.data[f][y][x + 1] & 0xff00) >> 8);		//Y2
			if (Y > 32)
			{
				ptr_c[off2] = ptr_g[x + 1] = Y;
				pix_count++;
			}
			else ptr_c[off2] = ptr_g[x + 1] = 0;
			ptr_c[off1 + 1] = ptr_c[off2 + 1] = (uchar)(out_img.data[f][y][x + 1] & 0x00ff);	//U
			
			x = x + 2;
			off1 = off1 + 6;
		}
		y++;
		
		ptr_c += off_c;
		ptr_g += off_g;
	}
	return pix_count;
}
