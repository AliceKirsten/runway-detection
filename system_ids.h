/** This example is public domain. */

/**
 * @file system_ids.h
 *
 * @brief System ID Numbers
 *
 * Defines the system id and component id
 *
 * Parse MavLINK messages
 *
 */

#ifndef SYSTEM_IDS_H_
#define SYSTEM_IDS_H_

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <common/mavlink.h>

#define INITIAL				0x1000
#define COMMAND				0x2000
#define ALT 				0x3000
#define COURSE				0x4000
#define FLYING				0x5000
#define LANDING				0x6000

#define NONE_MSG			0x0000
#define HEARTBIT_MSG		0x0001
#define SYSSTAT_MSG			0x0002
#define ATTITUDE_MSG		0x0004
#define GLOBALPOS_MSG		0x0008
#define RCSCALED_MSG		0x0010
#define RCRAW_MSG			0x0020
#define HILSTATE_MSG		0x0040
#define HILCONTROL_MSG		0x0080
#define STATUSTEXT_MSG		0x0100
#define CRIT_MSG			0x003F

// ------------------------------------------------------------------------------
//   Parameters
// ------------------------------------------------------------------------------

const uint8_t sysid            = 1;   // The vehicle's system ID (parameter: MAV_SYS_ID)
const uint8_t autopilot_compid = 11;  // The autopilot component (parameter: MAV_COMP_ID)
const uint8_t compid           = 1; 	// The offboard computer component ID */

struct Mavlink_Messages 
{
	int sysid;
	int compid;

	// Heartbeat
	mavlink_heartbeat_t heartbeat;

	// System Status
	mavlink_sys_status_t sys_status;
	
	// System Time
	mavlink_system_time_t time;
	
	// Attitude
	mavlink_attitude_t attitude;
	
	// GPS Raw Int
	mavlink_gps_raw_int_t gps_raw;
	
	// Raw IMU
	mavlink_raw_imu_t imu;
	
	// Scaled Pressure
	mavlink_scaled_pressure_t pressure;
	
	// Global Position
	mavlink_global_position_int_t global_position_int;
	
	// RC Cannels Scaled
	mavlink_rc_channels_scaled_t rc_channels_scaled;
	
	// RC Cannels Raw
	mavlink_rc_channels_raw_t rc_channels_raw;
	
	// Servo Output Raw
	mavlink_servo_output_raw_t servo_output_raw;
	
	// Mission Current
	mavlink_mission_current_t mission;
	
	// Nav Controller Output
	mavlink_nav_controller_output_t nav;
	
	// VFR HUD
	mavlink_vfr_hud_t vfr_hud;
	
	// HIL State
	mavlink_hil_state_t hil_state;
	
	// HIL Controls
	mavlink_hil_controls_t hil_controls;

	// Radio Status
	mavlink_radio_status_t radio_status;
	
	// Statustext
	mavlink_statustext_t statustext;
};

struct Drone_Params
{
	float yaw;					//absolute value of Drone yaw angle (in rad)
	float roll;					//absolute value of Drone roll angle (in rad)
	float pitch;				//absolute value of Drone pitch angle (in rad)
	float heading;				//filtered value of Drone course angle (in degrees)
	
	int32_t lat;				//absolute value of Drone latitude
	int32_t lon;				//absolute value of Drone longitude
	int32_t alt;				//absolute value of Drone altitude (in meters)
	int32_t rel_alt;			//relative value of Drone altitude (in meters)
	float 	g_speed;			//absolute value of Drone ground speed (in meters/sec)
	
	int16_t raw_RCchan5;		//raw value of command tumbler position
	int16_t scaled_RCchan5;		//scaled value of command tumbler position
	
	uint8_t mode;				//Drone mode
	uint8_t nav_mode;			//Drone navigation mode
	uint8_t system_status;		//Drone system status		
};

//parsing input messages and save it in structure	
uint16_t Parse_MavLINK_message(mavlink_message_t message);
//fill Drone parameters
void Set_Drone_Params(Drone_Params* Drone);
//print Drone parameters for debugging
void Print_Drone_Params(Drone_Params Drone);

#endif // SYSTEM_IDS_H_
