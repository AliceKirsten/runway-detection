/** This example is public domain. */

/**
 * @file serial_port.cpp
 *
 * @brief Serial interface functions
 *
 * Functions for opening, closing, reading and writing via serial ports
  *
 * @author Trent Lukaczyk, <aerialhedgehog@gmail.com>
 * @author Jaycee Lock, <jaycee.lock@gmail.com>
 *
 */
// ------------------------------------------------------------------------------
//   Includes
// ------------------------------------------------------------------------------

#include "serial_port.h"

// ------------------------------------------------------------------------------
//   Parameters
// ------------------------------------------------------------------------------

int  fd;									//file descriptor of serial port device
char BufRx[BUFLEN];							//input buffer			

void (*RxCallback)(char* Buf, int len); 	//calback declaration

// ------------------------------------------------------------------------------
//   Write to Serial
// ------------------------------------------------------------------------------
int write_MAVLink(mavlink_message_t &message)
{
	char buf[BUFLEN];

	// Send message to buffer
	unsigned int len = mavlink_msg_to_send_buffer((uint8_t*)buf, &message);

	// Write packet via serial link
	write(fd, buf, len);

	// Wait until all data has been written
	tcdrain(fd);

	return len;
}

// -----------------------------------------------------------------------------
//	 Signal SIGIO Handler
// -----------------------------------------------------------------------------
void SerialPortHandler(int status)
{
	int bytes, res;
	
	ioctl(fd, FIONREAD, &bytes); 				//how many bytes to read?
	if (bytes > 0)  							//this is RX event
	{
		memset((void*)BufRx, 0, BUFLEN);		//clean up buffer
		res = read(fd, BufRx, bytes); 			//read
		if (res < 1) printf("ERROR: Could not read from fd %d\n", fd);
		else RxCallback(BufRx, res);			//emit callback
	}
}

// ------------------------------------------------------------------------------
//   Open Serial Port
// ------------------------------------------------------------------------------
/**
 * throws EXIT_FAILURE if could not open the port
 */
void open_serial(char* uart_name, void (*RxFunc)(char*, int), mavlink_status_t* stat)
{
	struct sigaction saio;	
	sigset_t simask;
	
	RxCallback = RxFunc; //Store ptr of RX callback
 
 	// --------------------------------------------------------------------------
	//   OPEN PORT
	// --------------------------------------------------------------------------
	fd = _open_port(uart_name);

	// Check success
	if (fd == -1)
	{
		printf("failure, could not open port.\n");
		throw EXIT_FAILURE;
	}
	
	// --------------------------------------------------------------------------
	//	 SETUP SIGNALS
	// --------------------------------------------------------------------------
	sigfillset(&simask); 						//all signals
	saio.sa_handler = SerialPortHandler;		//setup handler func
	saio.sa_flags = 0;
	saio.sa_mask = simask; 						//lock all signals
	sigaction(SIGIO, &saio, NULL);  			//old fashion (without SA_SIGINFO)
	sigemptyset(&simask); 						//no signals
	sigaddset(&simask, SIGIO); 					//add SIGIO
	sigprocmask(SIG_UNBLOCK, &simask, NULL); 	//unlock SIGIO
	
	// --------------------------------------------------------------------------
	//   SETUP PORT
	// --------------------------------------------------------------------------
	bool success = _setup_port(BAUDRATE, 8, 1, false, false);

	// --------------------------------------------------------------------------
	//   CHECK STATUS
	// --------------------------------------------------------------------------
	if (!success)
	{
		printf("failure, could not configure port.\n");
		throw EXIT_FAILURE;
	}
	if (fd <= 0)
	{
		printf("Connection attempt to port %s with %d baud, 8N1 failed, exiting.\n", uart_name, BAUDRATE);
		throw EXIT_FAILURE;
	}

	// --------------------------------------------------------------------------
	//   CONNECTED!
	// --------------------------------------------------------------------------
	printf("Connected to %s with %d baud, 8 data bits, no parity, 1 stop bit (8N1)\n", uart_name, BAUDRATE);
	stat->packet_rx_drop_count = 0;

	return;
}

// ------------------------------------------------------------------------------
//   Close Serial Port
// ------------------------------------------------------------------------------
void close_serial()
{
	close(fd);
	printf("Port closed\n");
}

// ------------------------------------------------------------------------------
//   Helper Function - Open Serial Port File Descriptor
// ------------------------------------------------------------------------------
/**
 * Where the actual port opening happens, returns file descriptor 'fd'
 */
int _open_port(const char* port)
{
	// Open serial port
	// O_RDWR - Read and write
	// O_NOCTTY - Ignore special chars like CTRL-C
	fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);

	// Check for Errors
	if (fd == -1)
	{
		/* Could not open the port. */
		return(-1);
	}

	// Finalize
	else
	{
		fcntl(fd, F_SETOWN, getpid()); 			//allows the process to receive SIGIO
		fcntl(fd, F_SETFL, FASYNC | FNDELAY);
	}

	// Done!
	return fd;
}

// ------------------------------------------------------------------------------
//   Helper Function - Setup Serial Port
// ------------------------------------------------------------------------------
/**
 * Sets configuration, flags, and baud rate
 */
bool _setup_port(int baud, int data_bits, int stop_bits, bool parity, bool hardware_control)
{
	// Check file descriptor
	if(!isatty(fd))
	{
		printf("\nERROR: file descriptor %d is NOT a serial port\n", fd);
		return false;
	}

	// Read file descriptor configuration
	struct termios config;
	if(tcgetattr(fd, &config) < 0)
	{
		printf("\nERROR: could not read configuration of fd %d\n", fd);
		return false;
	}

	// Input flags - Turn off input processing
	// convert break to null byte, no CR to NL translation,
	// no NL to CR translation, don't mark parity errors or breaks
	// no input parity check, don't strip high bit off,
	// no XON/XOFF software flow control
	config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
	                    INLCR | PARMRK | INPCK | ISTRIP | IXON);

	// Output flags - Turn off output processing
	// no CR to NL translation, no NL to CR-NL translation,
	// no NL to CR translation, no column 0 CR suppression,
	// no Ctrl-D suppression, no fill characters, no case mapping,
	// no local output processing
	config.c_oflag &= ~(OCRNL | ONLCR | ONLRET |
	                     ONOCR | OFILL | OPOST);

	#ifdef OLCUC
  		config.c_oflag &= ~OLCUC;
	#endif

  	#ifdef ONOEOT
  		config.c_oflag &= ~ONOEOT;
  	#endif

	// No line processing:
	// echo off, echo newline off, canonical mode off,
	// extended input processing off, signal chars off
	config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

	// Turn off character processing
	// clear current char size mask, no parity checking,
	// no output processing, force 8 bit input
	config.c_cflag &= ~(CSIZE | PARENB);
	config.c_cflag |= (BAUDRATE | CS8);

	// One input byte is enough to return from read()
	// Inter-character timer off
	config.c_cc[VMIN]  = 1;
	config.c_cc[VTIME] = 0; 

	// Apply baudrate
	if (cfsetispeed(&config, BAUDRATE) < 0 || cfsetospeed(&config, BAUDRATE) < 0)
	{
		printf( "\nERROR: Could not set desired baud rate of %d Baud\n", baud);
		return false;
	}

	// Finally, apply the configuration
	if(tcsetattr(fd, TCSAFLUSH | TCSANOW, &config) < 0)
	{
		printf("\nERROR: could not set configuration of fd %d\n", fd);
		return false;
	}

	// Done!
	return true;
}









